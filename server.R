
function(input, output, session) {

  r <- reactiveValues(
    parcs_eolien_geom = react_df$parcs_eolien_geom,
    mat_eolien_geom = react_df$mat_eolien_geom,
    parcs_eolien_tableau = react_df$parcs_eolien_tableau,
    puissance_autor = react_df$puissance_autor,
    puissance_en_serv = react_df$puissance_en_serv,
    enedis_production_eol_photov = react_df$enedis_production_eol_photov
    )


  observeEvent(input$source, {

    message("Interrogation SIGLoire et Enedis en cours")

    data_eol <- update_eolien()
    r$parcs_eolien_geom <- data_eol$parcs_eolien_geom
    r$mat_eolien_geom <- data_eol$mat_eolien_geom
    r$parcs_eolien_tableau <- data_eol$parcs_eolien_tableau
    r$puissance_autor <- data_eol$puissance_autor
    r$puissance_en_serv <- data_eol$puissance_en_serv

    r$enedis_production_eol_photov <- update_enedis()

    react_df <- list(
      parcs_eolien_geom = data_eol$parcs_eolien_geom,
      mat_eolien_geom = data_eol$mat_eolien_geom,
      parcs_eolien_tableau = data_eol$parcs_eolien_tableau,
      puissance_autor = data_eol$puissance_autor,
      puissance_en_serv = data_eol$puissance_en_serv,
      enedis_production_eol_photov = r$enedis_production_eol_photov
      )


    save(
      update_eolien,
      synthese_dep,
      etat_parc_autorise_trim,
      etat_parc_construits_trim,
      etat_parc_instruction_trim,
      etat_parc_refuses_trim,
      etat_parc_trim,
      update_enedis,
      enedis_trimestre,
      ppe2,
      sraddet,
      custom_palette,
      react_df,
      file = "eolien_pv_trim_shiny.RData",
      version = 2)


    updateSelectInput(session,
                      inputId = "select_trim",
                      label = "selectionner un trimestre",
                      choices = quarter(ymd(seq.Date(as.Date("2015-01-01"), Sys.Date()-90 , by = "quarter")), type = "year.quarter"),
                      selected = quarter(ymd(Sys.Date()-120), type = "year.quarter"))

  })


  # tableau source DREAL ---------

  output$tableau_dreal <- renderTable({
      instruction <- etat_parc_instruction_trim(r$parcs_eolien_tableau, input$select_trim, indicateurs = TRUE)
      autorises <- etat_parc_autorise_trim(r$parcs_eolien_tableau, input$select_trim, indicateurs = TRUE)
      construit <- etat_parc_construits_trim(r$parcs_eolien_tableau, input$select_trim, indicateurs = TRUE)
      tableau <- instruction %>%
        full_join(autorises, by = "zone") %>%
        full_join(construit, by = "zone") %>%
        select(-starts_with("nb_mats")) %>%
        mutate_if(is.numeric, ~replace_na(.x, replace = 0)) %>%
        mutate(across(starts_with("nb_"), as.integer), across(starts_with("puiss_"), ~round(.x, 1))) %>%
        arrange(zone) %>%
        rename_with(~ gsub("_", " ", .x))
      return(tableau)
  })


  # graphe source DREAL ---------------

  output$graphe <- renderPlotly({
    p <- full_join(r$puissance_autor, r$puissance_en_serv) %>%
      filter(trim <= input$select_trim) %>%
      ggplot2::ggplot() +
      ggplot2::geom_line(ggplot2::aes(x = trim,
                                      y = puiss_MW_autor,
                                      group = 1,
                                      color = "autoris\u00e9e (en service et en construction)"
      ),
      linetype = "solid",
      size = 1.5) +
      ggplot2::geom_line(ggplot2::aes(x = trim,
                                      y = puiss_MW_en_serv,
                                      group = 1,
                                      color = "en service"),
                         linetype = "solid",
                         size = 1.5) +
      ggplot2::geom_path(data = ppe2,
                         ggplot2::aes(x = trim,
                                      y = estimation_ppe,
                                      group = 1,
                                      color = "PPE 2 (1/12 obj national)"),
                         linejoin = "round",
                         linetype = "dashed",
                         size = 1.5) +
      ggplot2::geom_line(data = sraddet,
                         ggplot2::aes(x = trim,
                                      y = estimation_sraddet,
                                      group = 1,
                                      color = "Projet SRADDET FdC 24%"),
                         linetype = "dashed",
                         size = 1.5) +
      scale_colour_manual(values = custom_palette) +
      ggplot2::labs(
        x = "",
        y = "puissance raccord\u00e9e (MW)",
        fill = "",
        caption = glue::glue('Source : SIGLoire / DREAL des Pays de la Loire')
      ) +
      ggplot2::scale_y_continuous(
        limits = c(0, NA),
        labels = scales::label_number(big.mark = " ", decimal.mark = ","),
        n.breaks = 10) +
      zoo::scale_x_yearqtr(format = "%Y", n = 10) +
      hrbrthemes::theme_ipsum() +
      ggplot2::theme(legend.position = c(0.7, 0.3),
                     legend.background = element_rect(fill = "white"),
                     axis.text.x = element_text(size = 10),
                     axis.title.y = element_text(face="bold", size = 14)
      ) +
      guides(color = guide_legend(title = "Puissances constat\u00e9es et projet\u00e9es",
                                  title.theme = element_text(size = 15))) +
      coord_cartesian(xlim = c(2001.1, 2050.4))

    ggplotly(p)
  })


  # tableau source Enedis ---------

  output$tableau_enedis <- renderTable({
    enedis_trimestre(r$enedis_production_eol_photov, trim = input$select_trim) %>%
      select(-trimestre) %>%
      mutate(across(ends_with("_nb"), as.integer), across(ends_with("puiss_"), ~round(.x, 1))) %>%
      rename_with(~ gsub("_", " ", .x))
  })


  # titre carte générale -----------

  output$trimestre_carte <- renderText({
    annee <- year(yq(input$select_trim))
    trimestre <- quarter(yq(input$select_trim))
    paste0("Carte des parcs et m\u00e2ts \u00e9oliens au ", trimestre, "e trimestre ", annee)
  })


  # carte des parcs et des mâts -----------

  observeEvent(c(input$source, input$select_trim), {
    r$parc_eolien <- r$parcs_eolien_geom %>%
      right_join(etat_parc_trim(r$parcs_eolien_tableau, input$select_trim), by = c("id_parc", "nom_parc"))
  })

  observeEvent(c(input$source, input$select_trim, r$parc_eolien), {
    r$mat_eolien <- r$mat_eolien_geom %>%
      inner_join(r$parc_eolien %>% st_drop_geometry(), by = c("id_parc", "nom_parc", "demandeur")) %>%
      mutate(etat_trim = fct_recode(etat_trim,
                                    "m\u00e2ts en cours d\'instruction " = "instruction",
                                    "m\u00e2ts autoris\u00e9s en construction" = "autorise",
                                    "m\u00e2ts autoris\u00e9s en service" = "construit",
                                    "m\u00e2ts refus\u00e9s" = "refus\u00e9"))
    })
  # subsiste un problème avec les parcs en doublon car une partie des mats a été autorisés et une partie refusée

  output$map_eolien <- renderLeaflet({
    req(r$parc_eolien, r$mat_eolien)
    (mapview(r$parc_eolien,
             alpha.regions = 0.7,
             legend = FALSE,
             layer.name = "parcs eoliens") +
       mapview(r$mat_eolien,
               zcol = c("etat_trim"),
               burst = TRUE,
               color = "black",
               col.regions = c("blue", "#3A9D23",  "magenta", "red"),
               # "m\u00e2ts autoris\u00e9s en construction", "m\u00e2ts autoris\u00e9s en service", "m\u00e2ts en cours d\'instruction ", "refusé
               alpha.regions = 1,
               legend = TRUE,
               layer.name = "mats eoliens"))@map %>%
      addFullscreenControl()
  })


  # titre général des onglets -----------

  output$trimestre_onglet <- renderText({
    annee <- year(yq(input$select_trim))
    trimestre <- quarter(yq(input$select_trim))
    entame <- ifelse(input$stock_flux, "Evolutions enregistr\u00e9es sur SIGLoire au ", " Etat des parcs \u00e9oliens SIGLoire en fin de ")
    paste0(entame, trimestre,"e trimestre ", annee) # a factoriser avec output$trimestre_carte
  })


  # parcs en instruction ----------

  observeEvent(c(input$source, input$select_trim, input$stock_flux, r$parcs_eolien_tableau),{
    r$etat_parc_instruction_trim_carte <- etat_parc_instruction_trim(etat = r$parcs_eolien_tableau,
                                                                     trimestre = input$select_trim,
                                                                     indicateurs = FALSE,
                                                                     nouveau = input$stock_flux) %>%
      select(id_parc, nom_parc, puissance_nominale_parc) %>%
      right_join(r$parcs_eolien_geom, ., by = 'id_parc')
  })

  observeEvent(c(input$source, input$select_trim, r$mat_eolien_geom, input$stock_flux),{
    r$mat_parc_instruction <- r$mat_eolien_geom %>% semi_join(r$etat_parc_instruction_trim_carte %>% st_drop_geometry())
  })

  output$map_parc_instruction <- renderLeaflet({
    (mapview(r$etat_parc_instruction_trim_carte, legend=FALSE) +
       mapview(r$mat_parc_instruction, col.regions = "magenta", legend = FALSE)
     )@map %>%
      addFullscreenControl()
  })

  output$liste_parc_instruction <- DT::renderDT(server = FALSE, {
    data <- etat_parc_instruction_trim(etat = r$parcs_eolien_tableau,
                                       trimestre = input$select_trim,
                                       indicateurs = FALSE,
                                       nouveau = input$stock_flux) %>%
      select(dep, id_parc, nom_parc, puissance_nominale_parc, nb_mats) %>%
      arrange(id_parc) %>%
      rename_with(~ gsub("_", " ", .x))
    DT::datatable(data, extensions = 'Buttons',
                  options = list(scrollX=TRUE,
                                 pageLength = 5,
                                 paging = TRUE,
                                 searching = TRUE,
                                 fixedColumns = TRUE,
                                 autoWidth = TRUE,
                                 ordering = TRUE,
                                 dom = 'Bfrtip',
                                 buttons = list(c(list(extend = 'csv', filename = paste0(Sys.Date(), '_instruction_', input$select_trim))))
                                 )
                  )
    })


  # parcs autorisés -----------

  observeEvent(c(input$source, input$select_trim, input$stock_flux, r$parcs_eolien_tableau),{
    r$etat_parc_autorise_trim_carte <- etat_parc_autorise_trim(etat = r$parcs_eolien_tableau,
                                                               trimestre = input$select_trim,
                                                               indicateurs = FALSE,
                                                               nouveau = input$stock_flux) %>%
      select(id_parc, nom_parc, puissance_nominale_parc) %>%
      right_join(r$parcs_eolien_geom, ., by = 'id_parc')
  })

  observeEvent(c(input$source, input$select_trim, r$mat_eolien_geom, input$stock_flux),{
    r$mat_parc_autorise <- r$mat_eolien_geom %>% semi_join(r$etat_parc_autorise_trim_carte %>% st_drop_geometry())
  })

  output$map_parc_autorise <- renderLeaflet({
    (mapview(r$etat_parc_autorise_trim_carte, legend=FALSE) +
       mapview(r$mat_parc_autorise, col.regions = "yellow", legend = FALSE)
     )@map %>%
      addFullscreenControl()
  })

  output$liste_parc_autorise <- DT::renderDT(server = FALSE, {
    data <- etat_parc_autorise_trim(etat = r$parcs_eolien_tableau,
                                    trimestre = input$select_trim,
                                    indicateurs = FALSE,
                                    nouveau = input$stock_flux) %>%
      select(dep, id_parc, nom_parc, puissance_nominale_parc, nb_mats) %>%
      arrange(id_parc) %>%
      rename_with(~ gsub("_", " ", .x))
    DT::datatable(data, extensions = 'Buttons',
                  options = list(scrollX=TRUE,
                                 pageLength = 5,
                                 paging = TRUE,
                                 searching = TRUE,
                                 fixedColumns = TRUE,
                                 autoWidth = TRUE,
                                 ordering = TRUE,
                                 dom = 'Bfrtip',
                                 buttons = list(c(list(extend = 'csv', filename = paste0(Sys.Date(), '_autorises_', input$select_trim))))
                                 )
                  )
    })


  # parcs en service ------------

  observeEvent(c(input$source, input$select_trim, input$stock_flux, r$parcs_eolien_tableau),{
    r$etat_parc_construits_trim_carte <- etat_parc_construits_trim(etat = r$parcs_eolien_tableau,
                                                                   trimestre = input$select_trim,
                                                                   indicateurs = FALSE,
                                                                   nouveau = input$stock_flux) %>%
      select(id_parc, nom_parc, puissance_nominale_parc) %>%
      right_join(r$parcs_eolien_geom, ., by = 'id_parc')
  })

  observeEvent(c(input$source, input$select_trim, r$mat_eolien_geom, input$stock_flux),{
    r$mat_parc_construits <- r$mat_eolien_geom %>% semi_join(r$etat_parc_construits_trim_carte %>% st_drop_geometry())
  })

  output$map_parc_construits <- renderLeaflet({
    (mapview(r$etat_parc_construits_trim_carte, legend=FALSE) +
       mapview(r$mat_parc_construits, col.regions = "#3A9D23", legend = FALSE)
     )@map %>%
      addFullscreenControl()
  })

  output$liste_parc_construits <- DT::renderDT(server = FALSE, {
    data <- etat_parc_construits_trim(etat = r$parcs_eolien_tableau,
                                      trimestre = input$select_trim,
                                      indicateurs = FALSE,
                                      nouveau = input$stock_flux) %>%
      select(dep, id_parc, nom_parc, puissance_nominale_parc, nb_mats) %>%
      arrange(id_parc) %>%
      rename_with(~ gsub("_", " ", .x))
    DT::datatable(data, extensions = 'Buttons',
                  options = list(scrollX=TRUE,
                                 pageLength = 5,
                                 paging = TRUE,
                                 searching = TRUE,
                                 fixedColumns = TRUE,
                                 autoWidth = TRUE,
                                 ordering = TRUE,
                                 dom = 'Bfrtip',
                                 buttons = list(c(list(extend = 'csv', filename = paste0(Sys.Date(), '_construits_', input$select_trim))))
                                 )
                  )
    })


  # parcs refusés ----------

  observeEvent(c(input$source, input$select_trim, input$stock_flux, r$parcs_eolien_tableau),{
    r$etat_parc_refuses_trim_carte <- etat_parc_refuses_trim(etat = r$parcs_eolien_tableau,
                                                             trimestre = input$select_trim,
                                                             indicateurs = FALSE,
                                                             nouveau = input$stock_flux) %>%
      select(id_parc, nom_parc, puissance_nominale_parc) %>%
      right_join(r$parcs_eolien_geom, ., by = 'id_parc')
  })

  observeEvent(c(input$source, input$select_trim, r$mat_eolien_geom, input$stock_flux),{
    r$mat_parc_refuses <- r$mat_eolien_geom %>% semi_join(r$etat_parc_refuses_trim_carte %>% st_drop_geometry())
  })

  observeEvent(c(input$source, input$select_trim, input$stock_flux, r$parcs_eolien_tableau),{
      r$test_etat_parc_refuses_trim_carte <- semi_join(etat_parc_refuses_trim(etat = r$parcs_eolien_tableau,
                                                                              input$select_trim,
                                                                              indicateurs = FALSE,
                                                                              nouveau = input$stock_flux) %>%
                                                         select(id_parc, nom_parc, puissance_nominale_parc),
                                                       r$parcs_eolien_geom,
                                                       by= 'id_parc') %>%
        nrow()
  })

  output$map_parc_refuses <- renderLeaflet({
    if(nrow(r$test_etat_parc_refuses_trim_carte == 0))({
      (mapview(r$mat_parc_refuses,legend = FALSE))@map %>%
        addFullscreenControl()
    })
    else({
      (mapview(r$etat_parc_refuses_trim_carte, legend = FALSE) +
         mapview(r$mat_parc_refuses, col.regions = "red", legend = FALSE)
       )@map %>%
        addFullscreenControl()
    })
  })

  output$liste_parc_refuses <- DT::renderDT(server = FALSE, {
    data <- etat_parc_refuses_trim(etat = r$parcs_eolien_tableau,
                                   trimestre = input$select_trim,
                                   indicateurs = FALSE,
                                   nouveau = input$stock_flux) %>%
      select(dep, id_parc, nom_parc, demandeur, puissance_nominale_parc, nb_mats) %>%
      arrange(id_parc) %>%
      rename_with(~ gsub("_", " ", .x))
    DT::datatable(data, extensions = 'Buttons',
                  options = list(scrollX=TRUE,
                                 pageLength = 5,
                                 paging = TRUE,
                                 searching = TRUE,
                                 fixedColumns = TRUE,
                                 autoWidth = TRUE,
                                 ordering = TRUE,
                                 dom = 'Bfrtip',
                                 buttons = list(c(list(extend = 'csv', filename = paste0(Sys.Date(), '_refuses_', input$select_trim))))
                                 )
                  )
    })

}
