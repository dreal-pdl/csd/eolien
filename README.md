# Explorations visant à faciliter l'élaboration de la [lettre trimestrielle éolien photovoltaïque](http://www.pays-de-la-loire.developpement-durable.gouv.fr/publication-de-la-lettre-regionale-eolien-et-a1226.html)
Données utilisées :   
- sigloire mats 1 https://catalogue.sigloire.fr/api/data/25823933-a92c-4d78-bacf-1cfcf339fd56
- sigloire mats 2 https://catalogue.sigloire.fr/api/data/bbc57d12-ce05-4ce4-a66f-bc17bff48d4f
- sigloire parcs https://catalogue.sigloire.fr/api/data/15468f65-9217-4edb-bfc3-4e68942097ce
- enedis trim : https://data.enedis.fr/explore/dataset/parc-des-installations-de-production-raccordees-par-departement-historique/

Première url de visualisation : https://julietteengelaere.shinyapps.io/explo_lettre_trim_eol_pv/
